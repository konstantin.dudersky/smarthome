"""Подключение к БД db_conf, db_data."""

from . import conf, data

__all__: list[str] = [
    "conf",
    "data",
]
