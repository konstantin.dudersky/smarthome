"""Константы."""

from enum import Enum, auto


class SystemVers(Enum):
    """Версии ОС."""

    ubuntu_22_04 = auto()
